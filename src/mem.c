#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
//void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    block_size size = (block_size) {region_actual_size(size_from_capacity((block_capacity){query}).bytes)};
    //мапим новый регион
    void* new_region = map_pages(addr, size.bytes, MAP_FIXED);
    //если не получилось замапить, пробуем замапить с флагом 0
    if(new_region == MAP_FAILED) {
        new_region = map_pages(addr, size.bytes, 0);
        //если не получилось, значит не получилось совсем
        if(new_region == MAP_FAILED) return REGION_INVALID;
    }
    //инициируем блоком
    block_init(new_region, size, NULL);

    return (struct region) {.addr = new_region, .size = size.bytes, .extends = (new_region == addr)};
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

static bool blocks_continuous (
        struct block_header const* fst,
        struct block_header const* snd );

/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
    struct block_header* current_block = HEAP_START;
    struct block_header* start_region = current_block;
    size_t current_region_size = 0;

    while (current_block){
        struct block_header* next_block = current_block->next;
        current_region_size += size_from_capacity(current_block->capacity).bytes;
        if(!next_block || !blocks_continuous(current_block, next_block)){
            munmap(start_region, current_region_size);
            start_region = next_block;
            current_region_size = 0;
        }
        current_block = next_block;
    }

}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */
//проверяет что блок пустой и достаточно большой, чтобы разделить на два
static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if(!block) return false;
    if(!block_splittable(block, query)){
        return false;
    }
    //указатель на новый разделенный блок
    void* split_block = block->contents + query;
    block_init(split_block, (block_size) {.bytes = block->capacity.bytes - query}, block->next);
    // ставим новый блок следующим
    block->next = (struct block_header*) split_block;
    //ставим запрашиваемую вместимость
    block->capacity.bytes = query;
    return true;
}


/*  --- Слияние соседних свободных блоков --- */
//возвращает указатель после конца блока
static void* block_after( struct block_header const* block ){
  return  (void*) (block->contents + block->capacity.bytes);
}
//проверяет последовательны ли блоки
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}
//проверяет что блоки пустые и последовательны
static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    if(!block || !block->next) return false;
    if(!mergeable(block, block->next)) return false;

    block_init(block, (block_size) {.bytes = size_from_capacity(block->capacity).bytes
                                             + size_from_capacity(block->next->capacity).bytes}, block->next->next);
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

struct block_search_result block_search_result_init(int type, struct block_header* block){
    return (struct block_search_result) {.type = type, .block = block};
}

static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    if(!block) return block_search_result_init(BSR_CORRUPTED, NULL);
    struct block_header* prev = NULL;
    while (block){
        //объединяем блоки пока получается
        while (try_merge_with_next(block))
            ;
        //если блока достаточно, то все супер
        if(block->is_free && block_is_big_enough(sz, block)){
            return block_search_result_init(BSR_FOUND_GOOD_BLOCK, block);
        }
        prev = block;
        block = block->next;
    }
    //дошли до конца и не нашли подходящий блок
    return block_search_result_init(BSR_REACHED_END_NOT_FOUND, prev);
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    //size_t size = size_max(query, BLOCK_MIN_CAPACITY);
    //ищем блок
    struct block_search_result result = find_good_or_last(block, query);
    if(result.type == BSR_FOUND_GOOD_BLOCK) {
        //пробуем разделить
        split_if_too_big(result.block, query);
        result.block->is_free = false;
    }
    return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    void* last_after = block_after(last);
    size_t size = size_max(query, BLOCK_MIN_CAPACITY);
    //аллоцируем новый регион
    struct region new_region = alloc_region(last_after, size);
    if(region_is_invalid(&new_region)){
        return NULL;
    }
    last->next = new_region.addr;
    //пробуем соединить со след блоком
    if(try_merge_with_next(last)) {
        //то есть получилось новый регион расширяет старый
        return last;
    }
    //новый регион выделился в другом месте
    return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    if(!heap_start) return NULL;
    size_t size = size_max(query, BLOCK_MIN_CAPACITY);
    //пробуем выделить блок в куче
    struct block_search_result result = try_memalloc_existing(size, heap_start);
    switch (result.type) {
        case BSR_REACHED_END_NOT_FOUND:
            //пробуем увеличить кучу и выделить блок еще раз
            result = try_memalloc_existing(size, grow_heap(result.block, size));
            if(result.type != BSR_FOUND_GOOD_BLOCK) {
                return NULL;
            }
            return result.block;
        case BSR_FOUND_GOOD_BLOCK:
            return result.block;
        case BSR_CORRUPTED:
            return NULL;
    }
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  //объединяем блоки пока получается
  while (try_merge_with_next(header))
      ;
}
