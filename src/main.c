#include "mem.h"
#include "mem_internals.h"

#define test_fail debug("test failed.\n")
#define test_success debug("test passed!\n")

//Обычное успешное выделение памяти.
static void test_memory_allocation(){
    debug("just memory allocation:\n");
    void* heap = heap_init(4096);
    if(!heap){
        debug("heap init failed\n");
        test_fail;
        return;
    }
    void* block = _malloc(1024);
    if(!block){
        debug("malloc failed\n");
        test_fail;
        return;
    }
    debug("malloc success:\n");
    debug_heap(stdout, heap);
//    _free(block);
//
//    debug("free block:\n");
//    debug_heap(stdout, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 4096}).bytes);
    test_success;
}

//Освобождение одного блока из нескольких выделенных.
static void test_freeing_one_block(){
    debug("free one block from several allocated ones:\n");
    void* heap = heap_init(4096);
    if(!heap){
        debug("heap init failed\n");
        test_fail;
        return;
    }
    debug("heap init:\n");
    debug_heap(stdout, heap);
    void* block1 = _malloc(1024);
    void* block2 = _malloc(1024);
    void* block3 = _malloc(1024);
    if(!block1 || !block2 || !block3) {
        debug("malloc blocks failed\n");
        test_fail;
        return;
    }
    debug("blocks allocated:\n");
    debug_heap(stdout, heap);

    _free(block2);
    if(!block1 || !block3){
        debug("freeing one block damaged another block\n");
        test_fail;
        return;
    }
    debug("second block free:\n");
    debug_heap(stdout, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 4096}).bytes);
    test_success;
}

//Освобождение двух блоков из нескольких выделенных.
static void test_freeing_two_blocks(){
    debug("free two blocks from several allocated ones:\n");
    void* heap = heap_init(4096);
    if(!heap){
        debug("heap init failed\n");
        test_fail;
        return;
    }
    debug("heap init:\n");
    debug_heap(stdout, heap);
    void* block1 = _malloc(1024);
    void* block2 = _malloc(1024);
    void* block3 = _malloc(1024);
    if(!block1 || !block2 || !block3) {
        debug("malloc blocks failed\n");
        test_fail;
        return;
    }
    debug("blocks allocated:\n");
    debug_heap(stdout, heap);

    _free(block1);
    _free(block3);
    if(!block2){
        debug("freeing blocks damaged another block\n");
        test_fail;
        return;
    }
    debug("first and third blocks is free:\n");
    debug_heap(stdout, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 4096}).bytes);
    test_success;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

//Память закончилась, новый регион памяти расширяет старый.
static void test_region_continuous(){
    debug("memory has run out, new memory region expands old one:\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    if(!heap){
        debug("heap init failed\n");
        test_fail;
        return;
    }
    debug("heap init:\n");
    debug_heap(stdout, heap);
    void* block1 = _malloc(REGION_MIN_SIZE);
    void* block2 = _malloc(1024);
    if(!block1 || !block2) {
        debug("malloc blocks failed\n");
        test_fail;
        return;
    }
    debug("blocks allocated:\n");
    debug_heap(stdout, heap);

    struct block_header* header1 = block_get_header(block1);
    struct block_header* header2 = block_get_header(block2);

    if(!header1 || !header2){
        debug("block header is null\n");
        test_fail;
        return;
    }

    if(header1->next != header2){
        debug("blocks is not continuous\n");
        test_fail;
        return;
    }
    size_t capacity = ((struct block_header*) heap)->capacity.bytes + header1->capacity.bytes + header2->capacity.bytes;

    munmap(heap, capacity);
    test_success;
}
//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
static void test_region_in_different_location(){
    debug("memory has run out, new region is allocated elsewhere:\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    if(!heap){
        debug("heap init failed\n");
        test_fail;
        return;
    }
    debug("heap init:\n");
    debug_heap(stdout, heap);
    void* block1 = _malloc(REGION_MIN_SIZE);
    if(!block1) {
        debug("malloc blocks failed\n");
        test_fail;
        return;
    }
    struct block_header* header1 = block_get_header(block1);
    if(!header1){
        debug("block header is null\n");
        test_fail;
        return;
    }
    debug("block allocated:\n");
    debug_heap(stdout, heap);

    //занимаем место после нашей кучи
    void* addr = header1->contents + header1->capacity.bytes;
    void* new_region = mmap(addr, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED , -1, 0 );
    debug("allocate another region\n");

    void* block2 = _malloc(REGION_MIN_SIZE);
    if(!block2) {
        debug("malloc blocks failed\n");
        test_fail;
        return;
    }
    struct block_header* header2 = block_get_header(block2);
    if(!header2){
        debug("block header is null\n");
        test_fail;
        return;
    }

    munmap(new_region, size_from_capacity((block_capacity){.bytes = REGION_MIN_SIZE}).bytes);
    size_t capacity = ((struct block_header*) heap)->capacity.bytes + header1->capacity.bytes + header2->capacity.bytes;
    munmap(heap, capacity);
    test_success;
}


int main(){

    debug("Test 1 ");
    test_memory_allocation();

    debug("Test 2");
    test_freeing_one_block();

    debug("Test 3");
    test_freeing_two_blocks();

    debug("Test 4");
    test_region_continuous();

    debug("Test 5");
    test_region_in_different_location();

    return 0;
}